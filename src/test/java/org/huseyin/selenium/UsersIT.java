package org.huseyin.selenium;

import org.apache.log4j.Logger;
import org.huseyin.selenium.helpers.LoginHelper;
import org.huseyin.selenium.pageobjects.UsersPage;
import org.huseyin.selenium.rules.WebDriverRule;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.ClassRule;

/**
 *
 * @author huseyin
 */
public class UsersIT {
    
    final static Logger logger = Logger.getLogger(UsersIT.class);
    
    final static String TEST_USER_1 = "Huseyin Ozyilmaz";
    final static String TEST_USER_2 = "Malcolm X";
    final static String TEST_USER_3 = "Martin Luther King";
    
    private UsersPage usersPage;
    
    @ClassRule 
    public static WebDriverRule webDriverRule = new WebDriverRule();
    
    @BeforeClass
    public static void login() {
        LoginHelper.login(webDriverRule.getDriver());
    }
    
    @Before
    public void setUp() {
        usersPage = PageFactory.initElements(webDriverRule.getDriver(), UsersPage.class);
        usersPage.goTo();
    }
    
    @Test
    public void addUser_NameAdded_ShouldBeDisplayedInUserList() {
        usersPage.addUser(TEST_USER_1);
        assertTrue(usersPage.findUser(TEST_USER_1));
    }
    
    @Test
    public void addUser_ThreeNameAdded_ShoulIncreaseUserListSizeByThree() {
        usersPage.addUser(TEST_USER_1);
        usersPage.addUser(TEST_USER_2);
        usersPage.addUser(TEST_USER_3);
        
        int currentListSize = usersPage.getUserList().size();
        int expectedListSize = currentListSize + 3;
        
        assertEquals(expectedListSize, usersPage.getRecentlyUpdatedUserList(expectedListSize).size());
    }
    
}
