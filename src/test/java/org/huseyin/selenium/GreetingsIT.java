package org.huseyin.selenium;

import org.huseyin.selenium.config.TestConfig;
import org.huseyin.selenium.helpers.LoginHelper;
import org.huseyin.selenium.pageobjects.GreetingsPage;
import org.huseyin.selenium.pageobjects.LoginPage;
import org.huseyin.selenium.rules.WebDriverRule;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author huseyin
 */
public class GreetingsIT {
    
    final static String TEST_PERSON_TO_GREET = "Edna Lambert";
    final static String GREETING = "Hello ";
    
    private GreetingsPage greetingsPage;
    
    @ClassRule 
    public static WebDriverRule webDriverRule = new WebDriverRule();
    
    @BeforeClass
    public static void login() {
        LoginHelper.login(webDriverRule.getDriver());
    }
    
    @Before
    public void setUp() {
        greetingsPage = PageFactory.initElements(webDriverRule.getDriver(), GreetingsPage.class);
        greetingsPage.goTo();
    }
    
    @Test
    public void default_PageVisited_ShouldDisplayTenNamesToBeGreeted() {
        assertEquals(10, greetingsPage.getGreetingCount());
    }
    
    @Test
    public void greet_PersonSelected_ShouldBeGreetedWithAPersonalisedMessage() {
        String expected = GREETING + TEST_PERSON_TO_GREET;
        assertEquals(expected, greetingsPage.greet(TEST_PERSON_TO_GREET));       
    }
}
