package org.huseyin.selenium.rules;

import org.huseyin.selenium.config.webdriver.WebDriverFactory;
import org.junit.rules.ExternalResource;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author huseyin
 */
public class WebDriverRule extends ExternalResource {
    
    private WebDriver webDriver;
    
    @Override
    protected void before() throws Throwable {
        webDriver = WebDriverFactory.getInstance().getDriver();
    }

    @Override
    protected void after() {
        WebDriverFactory.getInstance().removeDriver();
    }
    
    public WebDriver getDriver() {
        return webDriver;
    }
    
}
