package org.huseyin.selenium.rules;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author huseyin
 */
public class CaptureScreenshotOnFail extends TestWatcher {

    final static Logger logger = Logger.getLogger(CaptureScreenshotOnFail.class);

    private final WebDriver driver;

    public CaptureScreenshotOnFail(WebDriver driver) {
        this.driver = driver;
    }

    @Override
    protected void failed(Throwable e, Description description) {
        
        StringBuilder fileName = new StringBuilder();
        fileName.append(description.getClassName())
                .append(".")
                .append(description.getMethodName());

        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File targetFile = new File("target/screenshots/onfail/" + fileName + ".jpg");

        try {
            FileUtils.copyFile(scrFile, targetFile);
        } catch (IOException ex) {
            logger.error("Failed to copy screenshot file to target folder: " + ex.getMessage());
        }
    }
}
