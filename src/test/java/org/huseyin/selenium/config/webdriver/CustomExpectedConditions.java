package org.huseyin.selenium.config.webdriver;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

/**
 *
 * @author huseyin
 */
public class CustomExpectedConditions {

    public static ExpectedCondition<WebElement> visibilityOfOneOf(final List<WebElement> elements) {
        return new ExpectedCondition<WebElement>() {
            public WebElement apply(WebDriver driver) {
                for (WebElement element : elements) {
                    if (element.isDisplayed()) {
                        return element;
                    }
                }
                return null;
            }
        };
    }
    
    public static ExpectedCondition<Boolean> listHasSizeOf(final WebElement element, final int size) {
        return new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return element.findElements(By.tagName("li")).size() == size;                    
            }
        };
    }
    
    public static ExpectedCondition<Boolean> listHasAnElementWithText(final WebElement element, final String text) {
        return new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                boolean found = false;
                List<WebElement> listItems = element.findElements(By.tagName("li"));
                for(WebElement item : listItems) {
                    if(item.getText().equalsIgnoreCase(text)) {
                        return true;
                    }
                }
                return found;              
            }
        };
    }
    
    // This is not reliable as data transfer may happen quickly. 
    // Much more time may be consumed on data processing and rendering on the page 
    // therefore even jQuery.active is 0 data might not be ready yet on the page.
    // Much wiser is to use explicit wait for element to be shown on the page.
    public static ExpectedCondition<Boolean> ajaxCompleted() {
        return new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return (Boolean)((JavascriptExecutor) driver).executeScript("return jQuery.active == 0");        
            }
        };
    }

    public static ExpectedCondition<Boolean> urlContains(final String text) {
        return new ExpectedCondition<Boolean>() {
            private String currentUrl = "";

            @Override
            public Boolean apply(WebDriver driver) {
                currentUrl = driver.getCurrentUrl();
                return currentUrl.contains(text);
            }

            @Override
            public String toString() {
                return String.format("URL to contain \"%s\". Current URL: \"%s\"", text, currentUrl);
            }
        };
    }

    public static ExpectedCondition<Boolean> pageContainsText(final String text) {
        return new ExpectedCondition<Boolean>() {
            private String currentPage = "";

            @Override
            public Boolean apply(WebDriver driver) {
                currentPage = driver.getPageSource();
                return currentPage.contains(text);
            }

            @Override
            public String toString() {
                return String.format("Page to contain \"%s\"", text);
            }
        };
    }

}
