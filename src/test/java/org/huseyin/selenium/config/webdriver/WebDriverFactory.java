package org.huseyin.selenium.config.webdriver;

import java.util.concurrent.TimeUnit;
import org.huseyin.selenium.config.TestConfig;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ThreadGuard;

/**
 * Creates and destroys thread safe WebDriver
 * 
 * @author huseyin
 */
public class WebDriverFactory {

    private static final WebDriverFactory instance = new WebDriverFactory();

    private WebDriverFactory() {
    }

    public static WebDriverFactory getInstance() {
        return instance;
    }

    ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>() {
        @Override
        protected WebDriver initialValue() {
            WebDriver driver = ThreadGuard.protect(new FirefoxDriver());
            driver.manage().timeouts().implicitlyWait(Long.parseLong(TestConfig.SELENIUM_IMPLICITLY_WAIT), TimeUnit.SECONDS);
            return driver;
        }
    };
    
    public WebDriver getDriver() {
        return driver.get();
    }

    public void removeDriver() {
        driver.get().quit();
        driver.remove();
    }
}
