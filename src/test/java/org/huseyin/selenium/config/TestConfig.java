package org.huseyin.selenium.config;

import java.util.ResourceBundle;

/**
 *
 * @author huseyin
 */
public class TestConfig {
        
    private static final ResourceBundle bundle = ResourceBundle.getBundle("test");
    
    public static final String APP_URL = getPropertyValue("app.url");
    public static final String SELENIUM_IMPLICITLY_WAIT = getPropertyValue("selenium.implicitlyWait");
    
    public static final String TEST_EMAIL = getPropertyValue("test.email");
    public static final String TEST_PASSWORD = getPropertyValue("test.password");
    
    private static String getPropertyValue(String key){
        return bundle.getString(key);
    }
}
