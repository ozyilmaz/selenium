package org.huseyin.selenium;

import org.huseyin.selenium.rules.CaptureScreenshotOnFail;
import org.apache.log4j.Logger;
import org.huseyin.selenium.config.TestConfig;
import org.huseyin.selenium.pageobjects.SubscribePage;
import org.huseyin.selenium.rules.WebDriverRule;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author huseyin
 */
public class SubscribeIT {
    
    final static Logger logger = Logger.getLogger(SubscribeIT.class);
    
    final static String PAGE_HEADER = "Subscribe";
    final static String SUBSCRIPTION_SUCCESS_MSG = "Thanks for subscribing";
    final static String SUBSCRIPTION_FAILURE_MSG = "Subscription failed";
        
    private SubscribePage subscribePage;
    
    @ClassRule 
    public static WebDriverRule webDriverRule = new WebDriverRule();
    
    @Rule
    public CaptureScreenshotOnFail screenshot = new CaptureScreenshotOnFail(webDriverRule.getDriver());
    
    @Before
    public void setUp() {
        subscribePage = PageFactory.initElements(webDriverRule.getDriver(), SubscribePage.class);
        subscribePage.goTo();
    }
    
    @Test
    public void pageDisplayedSuccessfully() {
        assertEquals("Page cannot be displayed", PAGE_HEADER, subscribePage.getPageHeader());
    }
    
    // [the name of the tested service]_[expected input / tested state]_[expected behavior]
    @Test
    public void subscribe_ValidEmailGiven_ShouldShowSuccessMessage() {
        subscribePage.subscribe(TestConfig.TEST_EMAIL);
        assertEquals(SUBSCRIPTION_SUCCESS_MSG, subscribePage.getSubscriptionMessage());
    }
    
    @Test
    public void subscribe_NoEmailGiven_ShouldShowFailureMessage() {
        subscribePage.subscribe("");
        assertEquals(SUBSCRIPTION_FAILURE_MSG, subscribePage.getSubscriptionMessage());
    }
}
