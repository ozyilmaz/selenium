package org.huseyin.selenium;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;

/**
 *
 * @author huseyin
 */
public class JUnitTestMethodExecutionOrderLogIT {

    final static Logger logger = Logger.getLogger(JUnitTestMethodExecutionOrderLogIT.class);

    @BeforeClass
    public static void oneTimeSetUp() {
        logger.info("@BeforeClass");
    }

    @AfterClass
    public static void oneTimeTearDown() {
        logger.info("@AfterClass");
    }
    
    @Before
    public void beforeTest() {
        logger.info("@Before");
    }
    
    @After
    public void afterTest() {
        logger.info("@After");
    }

    @Test
    public void test() {
        logger.info("@Test");
    }
    
    @Rule
    public ExternalResource resource2 = new ExternalResource() {
        @Override
        protected void before() throws Throwable {
            logger.info("@Rule: Before");
        }

        @Override
        protected void after() {
            logger.info("@Rule: After");
        }
    };
    
    @ClassRule
    public static ExternalResource resource = new ExternalResource() {
        @Override
        protected void before() throws Throwable {
            logger.info("@ClassRule: Before");
        }

        @Override
        protected void after() {
            logger.info("@ClassRule: After");
        }
    };
}
