package org.huseyin.selenium.helpers;

import org.huseyin.selenium.config.TestConfig;
import org.huseyin.selenium.pageobjects.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author huseyin
 */
public class LoginHelper {
    
    public static void login(WebDriver driver) {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.goTo();
        loginPage.signIn(TestConfig.TEST_EMAIL, TestConfig.TEST_PASSWORD, false);
    }
}
