
package org.huseyin.selenium.pageobjects;

import java.lang.annotation.Annotation;
import org.huseyin.selenium.config.TestConfig;
import org.huseyin.selenium.config.webdriver.CustomExpectedConditions;
import org.huseyin.selenium.config.webdriver.PagePath;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author huseyin
 */
public abstract class AbstractPageObject {
    
    protected String path;
    private final WebDriver driver;
    private int waitTimeOutSeconds = 10;

    public AbstractPageObject(WebDriver driver) {
        this.driver = driver;
        Annotation pathAnnotation = getClass().getAnnotation(PagePath.class);
        if(pathAnnotation != null) {
            path = getClass().getAnnotation(PagePath.class).value();
        }
    }

    public void deleteAllCookies() {
        getDriver().manage().deleteAllCookies();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void goTo() {
        getDriver().navigate().to(TestConfig.APP_URL + path);
    }

    public String getPath() {
        return path;
    }
    
    public void setWaitTimeOutSeconds(int waitTimeOutSeconds) {
        this.waitTimeOutSeconds = waitTimeOutSeconds;
    }

    /**
     * Go to page and wait until url reflects
     * expected page (or timeout reached)
     */
    public void goToAndWait() {
        goTo();
        ensure_is_current();
    }

    public void ensure_is_current() {
        wait_until_true_or_timeout(CustomExpectedConditions.urlContains(path));
    }

    public boolean is_text_present(String text) {
        wait_until_true_or_timeout(CustomExpectedConditions.pageContainsText(text));
        return true;
    }
    
    public WebDriverWait waitMaximum(int timeoutInSeconds){
        return new WebDriverWait(driver, timeoutInSeconds);
    }
    

    /**
     * wait until condition is true or timeout kicks in
     */
    protected <V> V wait_until_true_or_timeout(ExpectedCondition<V> isTrue) {
        Wait<WebDriver> wait = new WebDriverWait(this.driver, waitTimeOutSeconds)
                .ignoring(StaleElementReferenceException.class);
        try {
            return wait.until(isTrue);
        } catch (TimeoutException rte) {
            throw new TimeoutException(rte.getMessage() + "\n\nPageSource:\n\n" + getDriver().getPageSource());
        }
    }

    public void setText(WebElement element, String text) {
        element.clear();
        element.sendKeys(text);
    }

    public void selectDropdownByText(WebElement element, String visibleText){
        Select filterSelect = new Select(element);
        waitForDropdownItems(element);
        filterSelect.selectByVisibleText(visibleText);
    }

    private void waitForDropdownItems(WebElement element) {
        WebDriverWait wait = new WebDriverWait(getDriver(),waitTimeOutSeconds );
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    protected WebElement find(By locator) {
        try {
            return getDriver().findElement(locator);
        } catch (NoSuchElementException ex) {
            throw new NoSuchElementException(ex.getMessage() + "\n\nPageSource:\n\n" + getDriver().getPageSource());
        }
    }
}
