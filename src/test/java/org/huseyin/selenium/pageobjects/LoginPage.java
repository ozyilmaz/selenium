/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.huseyin.selenium.pageobjects;

import org.huseyin.selenium.config.webdriver.PagePath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 *
 * @author huseyin
 */
@PagePath("")
public class LoginPage extends AbstractPageObject { 
    
    /**
     * All WebElements are identified by @FindBy annotation at the top of the class
     * 
     * Why:
     * Makes the code much more readable
     * We can use Page Factory, an inbuilt page object model concept for Selenium WebDriver 
     * and it is very optimised.
     * 
     * Exception:
     * Consider postponing finding elements at class initialisation if there is 
     * a high cost. In this case find elements in the specific function
     */
    @FindBy(id="email")
    WebElement loginEmail;
    
    @FindBy(id="pwd")
    WebElement loginPassword;
    
    @FindBy(id="remember")
    WebElement loginRemember;
    
    @FindBy(id="submit-btn")
    WebElement loginButton;
    
    @FindBy(id="simple-form-page-header")
    WebElement pageHeader;
    
    public LoginPage(WebDriver driver) {
        super(driver);
    }
        
    public void signIn(String email, String password, boolean remember) {
        
        loginEmail.clear();
        loginEmail.sendKeys(email);
        
        loginPassword.clear();
	loginPassword.sendKeys(password);
        
        if(loginRemember.isSelected()) {
            loginRemember.clear();
        }
        
        if(remember) {
            loginRemember.click();
        }
        
        loginButton.click();
    }
    
    public String getPageTitle() {
        return this.getDriver().getTitle();
    }
    
    public String getPageHeader() {
        return pageHeader.getText();
    }
}

