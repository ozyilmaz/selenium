package org.huseyin.selenium.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 *
 * @author huseyin
 */
public class LoginResultPage extends AbstractPageObject{
    
    @FindBy(how = How.TAG_NAME, using = "h1")
    WebElement loginMessage;
    
    public LoginResultPage(WebDriver driver) {
        super(driver);
    }
    
    public String getLoginResult() {
        return loginMessage.getText();
    }
}
