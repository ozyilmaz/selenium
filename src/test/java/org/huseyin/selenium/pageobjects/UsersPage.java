package org.huseyin.selenium.pageobjects;

import java.util.ArrayList;
import java.util.List;
import org.huseyin.selenium.config.webdriver.CustomExpectedConditions;
import org.huseyin.selenium.config.webdriver.PagePath;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 *
 * @author huseyin
 */
@PagePath("")
public class UsersPage extends AbstractPageObject {
    
    @FindBy(id="user-fullname")
    WebElement nameInput;
    
    @FindBy(id="add-user")
    WebElement addButton;
    
    @FindBy(id="user-list")
    WebElement userList;
    
            
    public UsersPage(WebDriver driver) {
        super(driver);
    }
        
    public void addUser(String fullname) {
        nameInput.clear();
        nameInput.sendKeys(fullname);
        addButton.click();
    }
    
    public List<String> getUserList() {
        List<String> users = new ArrayList<String>();
        
        List<WebElement> items = userList.findElements(By.tagName("li"));
        
        return users;
    }
    
    public List<String> getRecentlyUpdatedUserList(int expectedNumberOfUsers) {
        List<String> users = new ArrayList<String>();
        
        waitMaximum(20).until(CustomExpectedConditions.listHasSizeOf(userList, expectedNumberOfUsers));
        
        List<WebElement> items = userList.findElements(By.tagName("li"));
        for(WebElement elem : items) {
            users.add(elem.getText());
        }
        
        return users;
    }
    
    public boolean findUser(String name) {
        return waitMaximum(8).until(CustomExpectedConditions.listHasAnElementWithText(userList, name));
    }
    
}
