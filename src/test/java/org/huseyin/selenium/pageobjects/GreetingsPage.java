package org.huseyin.selenium.pageobjects;

import org.huseyin.selenium.config.webdriver.CustomExpectedConditions;
import org.huseyin.selenium.config.webdriver.PagePath;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 *
 * @author huseyin
 */
@PagePath("")
public class GreetingsPage extends AbstractPageObject {

    @FindBy(id="greeting-list")
    WebElement greetingList;
    
    @FindBy(id="user-greeting-modal")
    WebElement greetingModal;
    
    public GreetingsPage(WebDriver driver) {
        super(driver);
    }
    
    public int getGreetingCount() {
        waitMaximum(10).until(CustomExpectedConditions.listHasSizeOf(greetingList, 10));
        return greetingList.findElements(By.tagName("li")).size();
    }
    
    public String greet(String name) {
        String greetingMessage;
        By greetingPanelSelector = By.cssSelector("div > div > div.modal-body > p");
        
        waitMaximum(5).until(CustomExpectedConditions.listHasAnElementWithText(greetingList, name));
        greetingList.findElement(By.linkText(name)).click();
        waitMaximum(5).until(ExpectedConditions.visibilityOfElementLocated(greetingPanelSelector));
        greetingMessage = greetingModal.findElement(greetingPanelSelector).getText();
        
        return greetingMessage;
    }
    
}
