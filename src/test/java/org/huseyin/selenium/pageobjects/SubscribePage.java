package org.huseyin.selenium.pageobjects;

import java.util.List;
import org.huseyin.selenium.config.webdriver.CustomExpectedConditions;
import org.huseyin.selenium.config.webdriver.PagePath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 *
 * @author huseyin
 */
@PagePath("")
public class SubscribePage extends AbstractPageObject{
    
    @FindBy(id="subscribe-page-header")
    WebElement pageHeader;
    
    @FindBy(id="subscribe-email")
    WebElement subscriptionEmail;
    
    @FindBy(id="subscribe-button")
    WebElement subscriptionButton;
    
    @FindBy(how=How.CLASS_NAME, using="alert")
    List<WebElement> subscriptionMessages;
    
    public SubscribePage(WebDriver driver) {
        super(driver);
    }
    
    public String getPageHeader() {
        return pageHeader.getText();
    }
    
    public void subscribe(String email){
        subscriptionEmail.clear();
	subscriptionEmail.sendKeys(email);
        
        subscriptionButton.click();
    }
    
    public String getSubscriptionMessage(){
        WebElement msg = waitMaximum(10).until(CustomExpectedConditions.visibilityOfOneOf(subscriptionMessages));
        
        return msg.getText();
    }
}
