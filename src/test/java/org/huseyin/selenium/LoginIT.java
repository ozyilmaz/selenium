package org.huseyin.selenium;

import org.apache.log4j.Logger;
import org.huseyin.selenium.config.TestConfig;
import static org.junit.Assert.*;
import org.huseyin.selenium.pageobjects.LoginPage;
import org.huseyin.selenium.pageobjects.LoginResultPage;
import org.huseyin.selenium.rules.CaptureScreenshotOnFail;
import org.huseyin.selenium.rules.WebDriverRule;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author huseyin
 */
public class LoginIT {
    
    final static Logger logger = Logger.getLogger(LoginIT.class);
    
    final static String LOGIN_SUCCESS_MSG = "Login Successful";
    final static String LOGIN_FAILURE_MSG = "Login Unsuccessful";
    final static String PAGE_TITLE = "App";
    final static String PAGE_HEADER = "Login";
    
    private LoginPage loginPage;
    private LoginResultPage loginResultPage;

    @ClassRule 
    public static WebDriverRule webDriverRule = new WebDriverRule();
    
    @Rule
    public CaptureScreenshotOnFail screenshot = new CaptureScreenshotOnFail(webDriverRule.getDriver());
    
    @Before
    public void setUp() {
        loginPage = PageFactory.initElements(webDriverRule.getDriver(), LoginPage.class);
        loginPage.goTo();
    }

    @Test
    public void pageExist() {
        assertEquals("Page cannot be accessed", PAGE_TITLE, loginPage.getPageTitle());
    }
    
    @Test
    public void pageDisplayed() {
        assertEquals("Page cannot be displayed", PAGE_HEADER, loginPage.getPageHeader());
    }
    
    @Test
    public void canSignInWithAValidAccount() {
        loginResultPage = PageFactory.initElements(webDriverRule.getDriver(), LoginResultPage.class);
        loginPage.signIn(TestConfig.TEST_EMAIL, TestConfig.TEST_PASSWORD, true);
        
        assertEquals(LOGIN_SUCCESS_MSG, loginResultPage.getLoginResult());
    }
    
    @Test
    public void failSignInIfAccountIsInvalid() {
        loginResultPage = PageFactory.initElements(webDriverRule.getDriver(), LoginResultPage.class);
        loginPage.signIn("invalid@invalid.com", "invalid", true);
        
        assertEquals(LOGIN_FAILURE_MSG, loginResultPage.getLoginResult());
    }
    
}
