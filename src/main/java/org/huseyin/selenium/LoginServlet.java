package org.huseyin.selenium;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author huseyin
 */
public class LoginServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("text/html");
        response.setStatus(HttpServletResponse.SC_OK);
        
        String email = request.getParameter("email");
        String password = request.getParameter("pwd");
        
        try {
            Thread.sleep(1000l);
        } catch (InterruptedException e1) {
        }

        if("valid@valid.com".equals(email) && "valid".equals(password)) {
            response.getWriter().println("<h1>Login Successful</h1>");
        }
        else {
            response.getWriter().println("<h1>Login Unsuccessful</h1>");
        }
        
    }
}
