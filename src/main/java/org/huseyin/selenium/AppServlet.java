package org.huseyin.selenium;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
public class AppServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setStatus(HttpServletResponse.SC_OK);
        
        if("slow".equals(request.getParameter("load"))) {
            try {
                Thread.sleep(5000l);
            } catch (InterruptedException e1) {}
            
            response.getWriter().println("<h1>Slow Page</h1>");
        }
        else {
            response.getWriter().println("<h1>Somewhere else</h1>");
        }
        
        response.getWriter().println("session=" + request.getSession(true).getId());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("text/html");
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().println("<h1>Posted Successfully</h1>");
    }
}
